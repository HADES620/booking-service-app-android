package com.service.booking.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.service.booking.booking.R;

public class DiplomaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diploma);
        getSupportActionBar().hide();
    }
}
