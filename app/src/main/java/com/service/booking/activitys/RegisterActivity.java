package com.service.booking.activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.service.booking.booking.R;
import com.service.booking.models.PermissionSetting;
import com.service.booking.models.RequestCodeSet;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private PermissionSetting permissionSetting;

    private RoundedImageView mImageAvatarView;
    private MaterialEditText mFirstNameView;
    private MaterialEditText mLastNameView;
    private MaterialEditText mEmailView;
    private MaterialEditText mPasswordView;
    private MaterialEditText mRepeatPasswordView;
    private MaterialButton btn_register;

    private String mFirstname;
    private String mLastName;
    private String mEmail;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_register);

        initializUI();

    }

    private void initializUI() {
        permissionSetting =new PermissionSetting(RegisterActivity.this);
        mImageAvatarView = (RoundedImageView)findViewById(R.id.img_avatar);
        mFirstNameView = (MaterialEditText)findViewById(R.id.txt_firstName_register);
        mLastNameView = (MaterialEditText)findViewById(R.id.txt_lastName_register);
        mEmailView = (MaterialEditText)findViewById(R.id.txt_email_register);
        mPasswordView = (MaterialEditText)findViewById(R.id.txt_password_register);
        mRepeatPasswordView = (MaterialEditText)findViewById(R.id.txt_repassword_register);
        btn_register = (MaterialButton)findViewById(R.id.btn_register);
        mImageAvatarView.setOnClickListener(this);
        btn_register.setOnClickListener(this);


    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        if(permissionSetting.isPermissionStorage()) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RequestCodeSet.REQUEST_GALLERY);
        }
    }

    private void takePhotoFromCamera() {
        if(permissionSetting.isPermissionCamera()) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePictureIntent, RequestCodeSet.REQUEST_CAMERA);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeSet.REQUEST_CAMERA && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mImageAvatarView.setImageBitmap(imageBitmap);
        }

        if (requestCode == RequestCodeSet.REQUEST_GALLERY && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mImageAvatarView.setImageBitmap(imageBitmap);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_avatar : {
                    showPictureDialog();
                break;
            }
            case R.id.btn_register : {
                mFirstname = mFirstNameView.getText().toString();
                mLastName = mLastNameView.getText().toString();
                mEmail = mEmailView.getText().toString();
                mPassword = mPasswordView.getText().toString();
                Intent intent = new Intent(RegisterActivity.this, ProfessionActivity.class);
                startActivity(intent);
                finish();
                break;
            }
        }
    }



}
