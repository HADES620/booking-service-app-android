package com.service.booking.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dpizarro.pinview.library.PinView;
import com.hbb20.CountryCodePicker;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.service.booking.booking.R;

import java.util.regex.Pattern;

public class PhoneNumberVerifyActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = PhoneNumberVerifyActivity.this.getClass().getSimpleName();

    private ConstraintLayout content_step1;
    private ConstraintLayout content_step2;
    private ConstraintLayout content_step3;
    private MaterialButton btn_phoneNumber_next;
    private MaterialButton btn_Verify_back;
    private MaterialButton btn_Verify_continue;
    private MaterialEditText txt_phoneNumber;
    private CountryCodePicker txt_phoneCountryCode;
    private TextView txt_phoneNumber_view;
    private PinView pin_phoneVerifyCode;
    private ProgressBar progressBar;
    private TextView txt_verifyView;

    private String mPhoneNumber;
    private String mPhoneCountryCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_phone_number_verify);

        initializUI();
    }

    private void initializUI() {
        content_step1 = (ConstraintLayout)findViewById(R.id.step1);
        content_step2 = (ConstraintLayout)findViewById(R.id.step2);
        content_step3 = (ConstraintLayout)findViewById(R.id.step3);
        btn_phoneNumber_next = (MaterialButton)findViewById(R.id.btnNext_step1);
        btn_Verify_back = (MaterialButton)findViewById(R.id.btnBack_step2);
        btn_Verify_continue = (MaterialButton)findViewById(R.id.btnContinue_step2);
        txt_phoneNumber = (MaterialEditText)findViewById(R.id.txtPhoneNumber_step1);
        txt_phoneCountryCode = (CountryCodePicker)findViewById(R.id.txtCountryCode_step1);
        txt_phoneNumber_view = (TextView)findViewById(R.id.phonenumber_view_step2);
        pin_phoneVerifyCode = (PinView)findViewById(R.id.pinView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        txt_verifyView = (TextView)findViewById(R.id.txtVerification_step3);


        btn_phoneNumber_next.setOnClickListener(this);
        btn_Verify_back.setOnClickListener(this);
        btn_Verify_continue.setOnClickListener(this);
    }


    private void startTransition(final boolean show, final ConstraintLayout context_old, final ConstraintLayout context_new) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (show) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            int width = context_old.getWidth();

            context_old.animate().setDuration(shortAnimTime).translationX(
                    show ? -width : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    context_old.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            context_new.setX(width);
            context_new.animate().setDuration(shortAnimTime).translationX(
                    show ? 0 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    context_new.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {

            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            int width = context_old.getWidth();

            context_old.animate().setDuration(shortAnimTime).translationX(
                    show ? 0 : width).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    context_old.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
            context_new.setX(-width);
            context_new.animate().setDuration(shortAnimTime).translationX(
                    show ? 0 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    context_new.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNext_step1 : {
                boolean passFlag = true;
                if(txt_phoneNumber.getText().toString().isEmpty()) {
                    txt_phoneNumber.setError(getResources().getString(R.string.error_phoneNumber_empty));
                    passFlag = false;
                }else if(!isValidMobile(txt_phoneNumber.getText().toString())) {
                    txt_phoneNumber.setError(getResources().getString(R.string.error_phoneNumber_incorrect));
                    passFlag = false;
                }

                if(passFlag) {
                    mPhoneNumber = txt_phoneNumber.getText().toString();
                    mPhoneCountryCode = txt_phoneCountryCode.getSelectedCountryCodeWithPlus();
                    txt_phoneNumber_view.setText(mPhoneCountryCode + mPhoneNumber);
                    startTransition(true, content_step1, content_step2);
                }
                break;
            }
            case R.id.btnBack_step2 : {

                startTransition(false, content_step2, content_step1);
                break;
            }
            case R.id.btnContinue_step2 : {

                if(isVaildVerify(pin_phoneVerifyCode.getPinResults())) {
                    startTransition(true, content_step2, content_step3);
                    new PhoneVerifyWithFireBase().execute();
                }

            }
        }
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 8 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    private boolean isVaildVerify(String code) {
        boolean check = false;
        if(Pattern.matches("[0-9]+", code)) {
            check = true;
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_phoneCode_incorrect), Toast.LENGTH_LONG).show();
            check = false;
        }
        return check;
    }

    private class PhoneVerifyWithFireBase extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            int index =0;
            while (index <= 100) {
                try {
                    progressBar.setProgress(index);
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                index++;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d(TAG, String.valueOf(values));
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            txt_verifyView.setText(getResources().getString(R.string.txt_verified));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(PhoneNumberVerifyActivity.this, ProfessionTypeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 800);

            super.onPostExecute(s);
        }
    }
}
