package com.service.booking.activitys;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.service.booking.booking.R;

public class ProfessionTypeActivity extends AppCompatActivity implements View.OnClickListener{

    private ConstraintLayout btn_provider;
    private ConstraintLayout btn_customer;
    private ConstraintLayout blue_layout_provider;
    private ConstraintLayout blue_layout_customer;
    private ImageView check_provider;
    private ImageView check_customer;
    private TextView txt_provider;
    private TextView txt_customer;
    private MaterialButton btn_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profession_type);
        getSupportActionBar().hide();
        initializUI();
    }

    private void initializUI() {
        btn_provider = (ConstraintLayout)findViewById(R.id.btn_provider);
        blue_layout_provider = (ConstraintLayout)findViewById(R.id.content_provider_blue);
        check_provider = (ImageView)findViewById(R.id.img_check_provider);
        txt_provider = (TextView)findViewById(R.id.txt_provider);
        btn_customer = (ConstraintLayout)findViewById(R.id.btn_customer);
        blue_layout_customer = (ConstraintLayout)findViewById(R.id.content_customer_blue);
        check_customer = (ImageView)findViewById(R.id.img_check_customer);
        txt_customer = (TextView)findViewById(R.id.txt_customer);
        btn_continue = (MaterialButton)findViewById(R.id.btn_continue);
        btn_provider.setOnClickListener(this);
        btn_customer.setOnClickListener(this);
        btn_continue.setOnClickListener(this);

        txt_provider.setTextColor(getResources().getColor(R.color.colorBlue));
        blue_layout_provider.setVisibility(View.INVISIBLE);
        check_provider.setVisibility(View.INVISIBLE);
        txt_customer.setTextColor(getResources().getColor(R.color.colorBlue));
        blue_layout_customer.setVisibility(View.INVISIBLE);
        check_customer.setVisibility(View.INVISIBLE);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_provider : {
                txt_provider.setTextColor(getResources().getColor(R.color.colorWhite));
                blue_layout_provider.setVisibility(View.VISIBLE);
                check_provider.setVisibility(View.VISIBLE);
                txt_customer.setTextColor(getResources().getColor(R.color.colorBlue));
                blue_layout_customer.setVisibility(View.INVISIBLE);
                check_customer.setVisibility(View.INVISIBLE);
                break;
            }

            case R.id.btn_customer : {
                txt_provider.setTextColor(getResources().getColor(R.color.colorBlue));
                blue_layout_provider.setVisibility(View.INVISIBLE);
                check_provider.setVisibility(View.INVISIBLE);
                txt_customer.setTextColor(getResources().getColor(R.color.colorWhite));
                blue_layout_customer.setVisibility(View.VISIBLE);
                check_customer.setVisibility(View.VISIBLE);
                break;
            }

            case R.id.btn_continue : {
                Intent intent = new Intent(ProfessionTypeActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
            }
        }
    }
}
