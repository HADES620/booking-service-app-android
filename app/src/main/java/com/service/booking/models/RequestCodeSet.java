package com.service.booking.models;

public class RequestCodeSet {
    public static int REQUEST_PERMISSION_CAMERA = 100;
    public static int REQUEST_PERMISSION_STORAGE = 200;
    public static int REQUEST_PERMISSION_LOCATION = 300;

    public static int REQUEST_CAMERA = 101;
    public static int REQUEST_GALLERY = 201;
}
