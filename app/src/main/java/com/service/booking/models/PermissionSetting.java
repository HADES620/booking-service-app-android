package com.service.booking.models;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import pub.devrel.easypermissions.EasyPermissions;

public class PermissionSetting extends AppCompatActivity {

    private Context context;
    private boolean isStoragePermission = false;
    private boolean isLocationPermission = false;
    private boolean isCameraPermission = false;

    public PermissionSetting(Context context) {
        this.context = context;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == RequestCodeSet.REQUEST_PERMISSION_STORAGE) {
            if (permissions.length > 0 && grantResults.length > 0) {
                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    isStoragePermission = true;
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if(requestCode == RequestCodeSet.REQUEST_PERMISSION_LOCATION) {
            if (permissions.length > 0 && grantResults.length > 0) {
                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    isLocationPermission = true;
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if(requestCode == RequestCodeSet.REQUEST_PERMISSION_CAMERA) {
            if (permissions.length > 0 && grantResults.length > 0) {
                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    isCameraPermission = true;
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public boolean isPermissionStorage(){
        String[] storagePermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(context, storagePermissions)) {
            EasyPermissions.requestPermissions(context, "Access for storage.", RequestCodeSet.REQUEST_PERMISSION_STORAGE, storagePermissions);
        } else {
            isStoragePermission = true;
        }
        return isStoragePermission;
    }

    public boolean isPermissionLocation(){
        String[] locationPermissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (!EasyPermissions.hasPermissions(context, locationPermissions)) {
            EasyPermissions.requestPermissions(context, "Access for location.", RequestCodeSet.REQUEST_PERMISSION_LOCATION, locationPermissions);
        } else {
            isLocationPermission = true;
        }
        return isLocationPermission;
    }

    public boolean isPermissionCamera(){
        String[] cameraPermissions = {Manifest.permission.CAMERA};
        if (!EasyPermissions.hasPermissions(context, cameraPermissions)) {
            EasyPermissions.requestPermissions(context, "Access for camera.", RequestCodeSet.REQUEST_PERMISSION_CAMERA, cameraPermissions);
        } else {
            isCameraPermission = true;
        }
        return isCameraPermission;
    }

    public boolean getStoragePermission() {
        return isStoragePermission;
    }

    public boolean getLocationPermission() {
        return isLocationPermission;
    }

    public boolean getCameraPermission() {
        return isCameraPermission;
    }
}
